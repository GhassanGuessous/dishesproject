package com.guess;

public class CanalUtil {

    public static Canal createCanal(String canalName){
        String[] tokens = canalName.split(",");

        String name = tokens[0];
        String satelliteAbreviation = tokens[1].trim();
        Satellite satellite = getSatellite(satelliteAbreviation);
        System.out.println(satellite);
        return new Canal(name, satellite);
    }

    public static Satellite getSatellite(String abreviation){
        switch (abreviation){
            case "N" : return Satellite.NILESAT;
            case "H" : return Satellite.HOTBIRD;
            case "A" : return Satellite.ASTRA;
        }
        return null;
    }
}
