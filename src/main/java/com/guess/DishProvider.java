package com.guess;

public interface DishProvider {

    Dish createDish();
}
