package com.guess;

import java.util.ArrayList;
import java.util.List;

public class Dishes {

    private String firstDish;
    private String secondDish;
    private List<String> dishNames = new ArrayList<String>();
    private List<Dish> dishes;

    public Dishes(String firstDish) {
        this.firstDish = firstDish;
        dishNames.add(firstDish);
        this.dishes = createDishes();
    }

    public Dishes(String firstDish, String secondDish) {
        this.firstDish = firstDish;
        this.secondDish = secondDish;
        dishNames.add(firstDish);
        dishNames.add(secondDish);
        this.dishes = createDishes();
    }

    public List<Dish> createDishes(){
        List<Dish> dishes = new ArrayList<Dish>();
        for (String dishName : this.dishNames){
            Dish dish =  DishUtil.createDish(dishName);
            dishes.add(dish);
        }
        return dishes;
    }

    public String signal(String canalName) {
        Canal canal = CanalUtil.createCanal(canalName);
        return canal.caculateCanalSignal(dishes.get(1), canal);
    }

    public void move(String s, String s1) {
    }
}
