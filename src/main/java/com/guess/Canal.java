package com.guess;

public class Canal implements CanalProvider {

    private static final double UNIT_DEVIATION = 0.1;
    private static final int MAX_DEVIATION_TO_LOSE_SIGNAL = 1;
    private static final int MAX_STARS = 10;

    private String name;
    private Satellite satellite;
    private int signal;

    public Canal(String name, Satellite satellite) {
        this.name = name;
        this.satellite = satellite;
    }

    public void setSignal(int signal) {
        this.signal = signal;
    }

    public String caculateCanalSignal(Dish dish, Canal canal) {
        int nbrStarsToRemove;

        if(compareCardinalPoint(dish.getCardinalPoint(), canal.getCardinalPoint())){
            nbrStarsToRemove = (int)nbrStarsToRemove(dish.getFrequency(), canal.getFrequency());
            if(nbrStarsToRemove < MAX_STARS){
                return drawStars(MAX_STARS - nbrStarsToRemove);
            }
        }
        return "No signal !";
    }

    public double nbrStarsToRemove(double dishFrequancy, double satelliteFrequency){
        double diff = dishFrequancy - satelliteFrequency;
        return Math.round(Math.abs(diff*10));
    }

    public boolean compareCardinalPoint(String dishCardinal, String satelliteCardinal){
        if(dishCardinal.equals(satelliteCardinal))
            return true;
        return false;
    }

    public String drawStars(int nbrStars ){
        String stars = "|";
        for (int i = 0; i < nbrStars; i++) {
            stars += "*";
        }

        while(stars.length() <= 10){
            stars += ".";
        }

        return stars + "|";
    }

    public String getCardinalPoint() {
        return satellite.getCardinalPoint();
    }

    public double getFrequency(){
        return satellite.getFrequency();
    }
}
