package com.guess;

public class Dish {

    private int id;
    private double frequency;
    private String cardinalPoint;

    public Dish(int id, double frequency, String cardinalPoint) {
        this.id = id;
        this.frequency = frequency;
        this.cardinalPoint = cardinalPoint;
    }

    public double getFrequency() {
        return frequency;
    }

    public String getCardinalPoint() {
        return cardinalPoint;
    }
}
