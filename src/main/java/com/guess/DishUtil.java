package com.guess;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DishUtil {

    public static Dish createDish(String dishName){
        String[] tokens = dishName.split(",");

        int id = Integer.parseInt(tokens[0]);
        double frequency = extractFrequency(tokens[1]);
        String cardinalPoint = extractCardinal(tokens[1]);
        System.out.println("cardinal extracted " + cardinalPoint);
        return new Dish(id, frequency, cardinalPoint);
    }

    public static double extractFrequency(String str) {
        Matcher m = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)").matcher(str);
        while (m.find())
        {
            double d = Double.parseDouble(m.group(1));
            System.out.println("frequence extracted " + d);
            return d;
        }
        return 0.0;
    }

    public static String extractCardinal(String str) {
        return str.substring(str.length()-1, str.length());
    }
}
