package com.guess;

public enum Satellite {

    ASTRA("A", 19.2, "E"),
    HOTBIRD("H", 13.0, "E"),
    NILESAT("N", 7.0, "W");

    private String abreviation;
    private double frequency;
    private String cardinalPoint;

    Satellite(String abreviation, double frequency, String cardinalPoint) {
        this.abreviation = abreviation;
        this.frequency = frequency;
        this.cardinalPoint = cardinalPoint;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public double getFrequency() {
        return frequency;
    }

    public String getCardinalPoint() {
        return cardinalPoint;
    }
}
